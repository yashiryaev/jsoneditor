import org.scalamock.scalatest.AsyncMockFactory
import org.scalatest.concurrent.ScalaFutures.convertScalaFuture
import org.scalatest.{AsyncFlatSpec, FlatSpec, Matchers}
import project.exceptions.{InvalidLinkException, InvalidNumberOfArgumentsException, UnsafeLinkException}
import project.services.{Client, Reader, Requester, Scanner}

import scala.concurrent.Future

class ReaderTest extends FlatSpec with Matchers {

  implicit val ec = scala.concurrent.ExecutionContext.Implicits.global

  val requester = new Requester
  val scanner = new Scanner
  val client = new Client(requester)
  val reader = new Reader(client,scanner)

  "isLink" should "return false" in {
    reader.isLink("ausgdfukjasgdkfj") shouldBe false
  }

  "isLink" should "return true" in {
    reader.isLink("http://www.ex-ample.com/some/path?par=123") shouldBe true
  }
}

class AsyncReaderTest extends AsyncFlatSpec with Matchers with AsyncMockFactory {

  implicit val ec = scala.concurrent.ExecutionContext.Implicits.global

  val client: Client = stub[Client]
  val scanner: Scanner = stub[Scanner]
  val reader = new Reader(client,scanner)

  val link = "http://api.json.org/my/json/blahblahblah"
  val invalidLink = "asdasjkghjahdskj"
  val myRegex = "abc.*"
  val output = "out.txt"

  "read" should "return Future(tuple)" in {
    (scanner.scanString _).when().returns(List(link, myRegex, output))
    (client.getValid _).when(link).returns(Future.successful(true))

    reader.read().map(x => {
      x shouldBe (link, myRegex, output)
    })
  }

  "read" should "return Future.failed(InvalidNumberOfArgumentsException)" in {
    (scanner.scanString _).when().returns(List(link, myRegex))

    reader.read().failed.futureValue shouldBe an[InvalidNumberOfArgumentsException]
  }

  "read" should "return Future.failed(UnsafeLinkException)" in {
    (scanner.scanString _).when().returns(List(link, myRegex, output))
    (client.getValid _).when(link).returns(Future.failed(new UnsafeLinkException))

    reader.read().failed.futureValue shouldBe an[UnsafeLinkException]
  }

  "read" should "return Future.failed(InvalidLinkException)" in {
    (scanner.scanString _).when().returns(List(invalidLink, myRegex, output))

    reader.read().failed.futureValue shouldBe an[InvalidLinkException]
  }

  "isValidLink" should "return Future(true)" in {
    (client.getValid _).when(link).returns(Future.successful(true))

    reader.isValidLink(link).map(x => assert(x))
  }

  "isValidLink" should "return Future.failed(InvalidLinkException)" in {
    reader.isValidLink(invalidLink).failed.futureValue shouldBe an[InvalidLinkException]
  }

  "isValidLink" should "return Future.failed(UnsafeLinkException)" in {
    (client.getValid _).when(link).returns(Future.failed(new UnsafeLinkException))

    reader.isValidLink(link).failed.futureValue shouldBe an[UnsafeLinkException]
  }

  "isSafeLink" should "return Future(true)" in {
    (client.getValid _).when(link).returns(Future.successful(true))

    reader.isSafeLink(link).map(x => assert(x))
  }

  "isSafeLink" should "return Future.failed(UnsafeLinkException)" in {
    (client.getValid _).when(link).returns(Future.failed(new UnsafeLinkException))

    reader.isSafeLink(link).failed.futureValue shouldBe an[UnsafeLinkException]
  }
}
