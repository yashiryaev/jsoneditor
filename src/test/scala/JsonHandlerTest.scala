import io.circe.Json
import org.scalamock.scalatest.{AsyncMockFactory, MockFactory}
import org.scalatest.{AsyncFlatSpec, FlatSpec, Matchers}
import project.services.{Client, JsonHandler, Requester}
import io.circe.parser._
import org.scalatest.concurrent.ScalaFutures.convertScalaFuture
import project.exceptions.EmptyJsonException

import scala.concurrent.{ExecutionContext, Future}
import scala.io.Source

class JsonHandlerTest extends FlatSpec with Matchers{

  implicit val ec: ExecutionContext = scala.concurrent.ExecutionContext.Implicits.global
  val requester = new Requester
  val client = new Client(requester)
  val jsonHandler = new JsonHandler(client)
  val link = "http://api.json.org/my/json/blahblahblah"
  val myRegex = "abc.*"
  val output = "out.txt"

  val json: Json =
    parse(
    """
      |{
      |   "key1" : "string1",
      |   "key2" : {
      |     "key3" : "string4",
      |     "abcdef" : "some",
      |     "key5" : [
      |       {
      |         "key6" : "string6",
      |         "abc" : "string7"
      |       },
      |       {
      |         "ab" : "string"
      |       }
      |     ]
      |   }
      |}
      |""".stripMargin).getOrElse(Json.Null)

  val convertedJson: Json =
    parse(
    """
      |{
      |   "key1" : "string1",
      |   "key2" : {
      |     "key3" : "string4",
      |     "key5" : [
      |       {
      |         "key6" : "string6"
      |       },
      |       {
      |         "ab" : "string"
      |       }
      |     ]
      |   }
      |}
      |""".stripMargin).getOrElse(Json.Null)

  "convertJson" should "return convertedJson" in {
    jsonHandler.convertJson(json,myRegex.r) shouldBe convertedJson
  }

  "printConvertedJson" should "print to file" in {
    jsonHandler.printConvertedJson(json, myRegex, output)

    val src = Source.fromFile(output)
    val content = parse(src.getLines().mkString).getOrElse(Json.Null)
    src.close()

    content shouldBe convertedJson
  }
}

class AsyncJsonHandlerTest extends AsyncFlatSpec with Matchers with AsyncMockFactory {

  implicit val ec: ExecutionContext = scala.concurrent.ExecutionContext.Implicits.global

  val client: Client = stub[Client]
  val jsonHandler = new JsonHandler(client)
  val link = "http://api.json.org/my/json/blahblahblah"
  val myRegex = "abc.*"
  val output = "out.txt"

  val json: Json =
    parse(
      """
        |{
        |   "key1" : "string1",
        |   "key2" : {
        |     "key3" : "string4",
        |     "abcdef" : "some",
        |     "key5" : [
        |       {
        |         "key6" : "string6",
        |         "abc" : "string7"
        |       },
        |       {
        |         "ab" : "string"
        |       }
        |     ]
        |   }
        |}
        |""".stripMargin).getOrElse(Json.Null)

  val convertedJson: Json =
    parse(
      """
        |{
        |   "key1" : "string1",
        |   "key2" : {
        |     "key3" : "string4",
        |     "key5" : [
        |       {
        |         "key6" : "string6"
        |       },
        |       {
        |         "ab" : "string"
        |       }
        |     ]
        |   }
        |}
        |""".stripMargin).getOrElse(Json.Null)

  "handle" should "return Unit" in {
    (client.getJson _).when(link).returns(Future(json))

    jsonHandler.handle((link,myRegex,output)).map(
      _ shouldBe a[Unit]
    )
  }

  "handle" should "throw an EmptyJsonException" in {
    (client.getJson _).when(link).returns(Future.successful(Json.Null))

    jsonHandler.handle((link,myRegex,output)).failed.futureValue shouldBe an[EmptyJsonException]
  }
}
