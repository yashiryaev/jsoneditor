import akka.http.scaladsl.model.{HttpEntity, HttpHeader, HttpResponse, StatusCodes}
import com.typesafe.config.{Config, ConfigFactory}
import io.circe.Json
import io.circe.parser._
import org.scalamock.scalatest.AsyncMockFactory
import org.scalatest.concurrent.ScalaFutures.convertScalaFuture
import org.scalatest.{AsyncFlatSpec, Matchers}
import project.exceptions.UnsafeLinkException
import project.services.{Client, Requester}

import scala.concurrent.{ExecutionContext, Future}

class ClientTest extends AsyncFlatSpec with Matchers with AsyncMockFactory {

  implicit val ec: ExecutionContext = scala.concurrent.ExecutionContext.Implicits.global

  val requester: Requester = stub[Requester]
  val client = new Client(requester)

  val link = "http://api.json.org/my/json/blahblahblah"
  val myRegex = "abc.*"
  val output = "out.txt"

  val config: Config = ConfigFactory.load("application")
  val userName: String = config.getString("yandexSafeApiUserName")
  val myKey: String = config.getString("yandexSafeApiKey")
  val body: String =
    s"""
       |{
       | "client": {
       |    "clientId": "${userName}",
       |    "clientVersion": "{1.1.1}"
       |  },
       |  "threatInfo": {
       |    "threatTypes": ["THREAT_TYPE_UNSPECIFIED"],
       |    "platformTypes": ["PLATFORM_TYPE_UNSPECIFIED"],
       |    "threatEntryTypes": ["URL"],
       |    "threatEntries": [
       |      {"url": "${link}"}
       |    ]
       |  }
       |}
       |""".stripMargin

  val emptyResponse: HttpResponse = HttpResponse(StatusCodes.OK, Seq(), HttpEntity.Empty)
  val notEmptyResponse: HttpResponse = HttpResponse(StatusCodes.OK, Seq(), HttpEntity(body))

  "getValid" should "return Future(true)" in {
    (requester.validationRequest _).when(body,myKey).returns(Future.successful(emptyResponse))

    client.getValid(link).map(x => assert(x))
  }

  "getValid" should "return Future.failed(UnsafeLinkException)" in {
    (requester.validationRequest _).when(body,myKey).returns(Future.successful(notEmptyResponse))

    client.getValid(link).flatMap(x => Future(x)).failed.futureValue shouldBe an[UnsafeLinkException]
  }

  "decodeValidatingRequest" should "return Future(true)" in {
    client.decodeValidatingRequest(emptyResponse).map(x => assert(x))
  }

  "decodeValidatingRequest" should "return Future.failed" in {
    client.decodeValidatingRequest(notEmptyResponse).failed.futureValue shouldBe an[UnsafeLinkException]
  }

  "getJson" should "return Future(not null Json)" in {
    (requester.jsonRequest _).when(link).returns(Future.successful(notEmptyResponse))

    client.getJson(link).map(x => assert(x == parse(body).getOrElse(Json.Null)))
  }

  "getJson" should "return Future(Json.Null)" in {
    (requester.jsonRequest _).when(link).returns(Future.successful(emptyResponse))

    client.getJson(link).map(x => assert(x == Json.Null))
  }

  "decodeGetResponse" should "return Future(not null Json)" in {
    client.decodeGetResponse(notEmptyResponse).map(x => assert(x == parse(body).getOrElse(Json.Null)))
  }

  "decodeGetResponse" should "return Future(Json.Null)" in {
    client.decodeGetResponse(emptyResponse).map(x => assert(x == Json.Null))
  }
}
