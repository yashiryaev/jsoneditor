package project.exceptions

class InvalidNumberOfArgumentsException extends Exception("invalid number of arguments")
