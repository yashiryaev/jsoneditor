package project.services

import scala.io.StdIn.readLine

/**
 * Считывает из консоли строку с аргументами
 *
 * Вынесено сюда, чтобы протестировать Reader
 */
class Scanner {
  def scanString: List[String] = {
    println("Enter link, regexp and output file name separated by space: ")
    readLine().split(' ').toList
  }
}
