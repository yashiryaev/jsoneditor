package project.services

import project.exceptions.{InvalidLinkException, InvalidNumberOfArgumentsException}

import scala.concurrent.{ExecutionContext, Future}

/**
 * Обрабатывает считанные в Scanner значения
 * @param client
 * @param scanner
 * @param ec
 */
class Reader(client: Client, scanner: Scanner)(implicit ec: ExecutionContext) {
  def read(): Future[(String, String, String)] = {
    val arguments = scanner.scanString

    if (arguments.size >= 3) {
      val link :: reg :: output :: other = arguments
      isValidLink(link).map {
        case true => (link, reg, output)
        case _ => throw new Exception("Something went wrong")
      }
    } else {
      Future.failed(new InvalidNumberOfArgumentsException)
    }
  }

  def isValidLink(link: String): Future[Boolean] = {
    if (isLink(link)) {
      isSafeLink(link)
    } else {
      Future.failed(new InvalidLinkException)
    }
  }

  def isLink(link: String): Boolean = {
    // ------------------ http ------------- www | smth --- smth123. ----- ru|smth --- /path ------ ? key = value
    val linkFormat = "((http|https)://)?(([a-z0-9-]+|www)\\.)?([a-z0-9-]+\\.)+[a-z]+(/([a-z0-9.-])+)*(\\?(([a-z0-9]+)=([a-z0-9]+)&)*(([a-z0-9]+)=([a-z0-9]+)))*".r
    linkFormat.matches(link)
  }

  def isSafeLink(link: String): Future[Boolean] = { // запрос на сервис, который производит валидацию
    client.getValid(link)
  }
}
