package project.services
import java.io.PrintWriter

import io.circe.{Json, JsonObject}
import io.circe.syntax._
import project.exceptions.EmptyJsonException

import scala.concurrent.{ExecutionContext, Future}
import scala.util.matching.Regex

class JsonHandler(client: Client)(implicit ec: ExecutionContext) {
  def handle(arguments: (String, String, String)): Future[Unit] = {
    client.getJson(arguments._1).map(x => {
      if (x.isNull) throw new EmptyJsonException
      else printConvertedJson(x, arguments._2, arguments._3)
    })
  }

  def printConvertedJson(json: Json, regex: String, output: String): Unit = {
    val updatedJson = convertJson(json, regex.r)

    val printer = new PrintWriter(output)
    printer.write(updatedJson.toString())
    printer.close()
  }

  def convertJson(json: Json, regex: Regex): Json = {

    def vectorRun(vector: Vector[Json]): Vector[Json] = {
      vector.map(convertJson(_,regex))
    }

    val map: Map[String, Json] = json.asObject.getOrElse(JsonObject.empty).toMap
    val keys: Iterable[String] = map.keys.filter(regex.matches(_))

    map.removedAll(keys).map(p => {
      p._2.asObject match {
        case Some(_) => (p._1, convertJson(p._2,regex))
        case None =>
          p._2.asArray match {
            case Some(_) => (p._1,p._2.mapArray(vectorRun).asJson)
            case None => p
          }
      }
    }).asJson
  }
}
