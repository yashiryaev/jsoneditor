package project.services

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpMethods, HttpRequest, HttpResponse}

import scala.concurrent.Future

/**
 * Класс, в котором выполняются запросы
 */
class Requester {
  implicit val actorSystem: ActorSystem = ActorSystem()

  def validationRequest(body: String, key: String): Future[HttpResponse] =
    Http().singleRequest(
      HttpRequest(
        HttpMethods.POST,
        uri = s"https://sba.yandex.net/v4/threatMatches:find?key=${key}",
        entity = body)
    )

  def jsonRequest(link: String): Future[HttpResponse] = Http().singleRequest(HttpRequest(uri = link))
}
