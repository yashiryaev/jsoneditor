package project.services

import akka.actor.ActorSystem
import akka.http.scaladsl.model.HttpResponse
import com.typesafe.config.{Config, ConfigFactory}
import akka.http.scaladsl.unmarshalling._
import io.circe.Json
import io.circe.parser._
import project.exceptions.UnsafeLinkException

import scala.concurrent.{ExecutionContext, Future}

/**
 * Класс, который преобразует выполненные запросы
 *
 * (Раньше в нем и запрашивалось, но вынес запросы в Requester, чтобы можно было тестировать этот класс)
 *
 * @param requester
 * @param ec
 */
class Client(requester: Requester)(implicit ec: ExecutionContext) {
  implicit val actorSystem: ActorSystem = ActorSystem()
  val config: Config = ConfigFactory.load("application")

  def getValid(link: String): Future[Boolean] = {
    val userName = config.getString("yandexSafeApiUserName")

    val body =
      s"""
        |{
        | "client": {
        |    "clientId": "${userName}",
        |    "clientVersion": "{1.1.1}"
        |  },
        |  "threatInfo": {
        |    "threatTypes": ["THREAT_TYPE_UNSPECIFIED"],
        |    "platformTypes": ["PLATFORM_TYPE_UNSPECIFIED"],
        |    "threatEntryTypes": ["URL"],
        |    "threatEntries": [
        |      {"url": "${link}"}
        |    ]
        |  }
        |}
        |""".stripMargin

    val key: String = config.getString("yandexSafeApiKey")

    val response: Future[HttpResponse] = requester.validationRequest(body, key)

    response.flatMap(decodeValidatingRequest)
  }

  def decodeValidatingRequest(httpResponse: HttpResponse): Future[Boolean] = { // если приходит пустой - значит безопасный
    Unmarshal(httpResponse).to[String].map(x => {
      if (x == "" || x == "{}") true
      else throw new UnsafeLinkException
    })
  }

  def getJson(link: String): Future[Json] = {
    val response: Future[HttpResponse] = requester.jsonRequest(link)

    response.flatMap(decodeGetResponse)
  }

  def decodeGetResponse(httpResponse: HttpResponse): Future[Json] = {
    Unmarshal(httpResponse).to[String].map(x => {
      parse(x).getOrElse(Json.Null)
    })
  }

}
