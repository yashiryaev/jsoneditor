package project

import project.services.{Client, JsonHandler, Reader, Requester, Scanner}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.sys.exit

object Main extends App {
  val requester = new Requester
  val scanner = new Scanner
  val client = new Client(requester)
  val reader = new Reader(client,scanner)
  val jsonHandler = new JsonHandler(client)

  val arguments: Future[(String, String, String)] = reader.read().recover({
    case exception: Exception =>
      println(exception.getMessage)
      exit(0)
  })

  arguments.flatMap(jsonHandler.handle).recover({
    case exception: Exception =>
      println(exception.getMessage)
      exit(0)
  }).map(x => exit(0))
}
